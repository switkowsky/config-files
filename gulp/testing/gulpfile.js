var gulp = require('gulp');
var notify = require('gulp-notify');
var phpspec = require('gulp-phpspec');

gulp.task('test', function () {
    gulp.src('spec/**/*.php')
        .pipe(phpspec('', {
            notify: true,
            clear: true,
            verbose: 'v'
        }))
        .on('error', notify.onError({
            title: 'Such error',
            icon: __dirname + '/wow_error.jpg',
            message: 'Tests covered in red.'
        }))
        .pipe(notify({
            title: 'Such success',
            icon: __dirname + '/wow.png',
            message: 'I see green. Sometimes.'
        }));
});

gulp.task('watch', function () {
    gulp.watch(['spec/**/*.php', 'src/**/*.php'], ['test']);
});

gulp.task('default', ['test', 'watch']);